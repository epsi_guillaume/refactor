package com.mycompany.refactor;

import static org.junit.Assert.*;

import org.junit.Test;

public class MovieTest {

    @Test
    public void testGetTitleRegularMovie() {
        Movie m = new RegularMovie("Test");
        assertEquals("Test", m.getTitle());
    }

    @Test
    public void testGetTitleChildrenMovie() {
        Movie m = new ChildrenMovie("Test");
        assertEquals("Test", m.getTitle());
    }

    @Test
    public void testGetTitleNewReleaseMovie() {
        Movie m = new NewReleaseMovie("Test");
        assertEquals("Test", m.getTitle());
    }

    @Test
    public void testGetFrequentRenterPointsRegularMovie() {
        Movie m = new RegularMovie("Test");
        assertTrue(m.getFrequentRenterPoints() == 1);
    }

    @Test
    public void testGetFrequentRenterPointsChildreMovie() {
        Movie m = new ChildrenMovie("Test");
        assertTrue(m.getFrequentRenterPoints() == 1);
    }

    @Test
    public void testGetFrequentRenterPointsNewReleaseMovie() {
        Movie m = new NewReleaseMovie("Test");
        assertTrue(m.getFrequentRenterPoints() == 2);
    }

    @Test
    public void testGetPriceCodeRegularMovie() {
        Movie m = new RegularMovie("Test");
        assertTrue(m.getPriceByDay(2) == 2);
    }

    @Test
    public void testGetPriceCodeChildrenMovieQuatreJours() {
        Movie m = new ChildrenMovie("Test");
        assertTrue(m.getPriceByDay(4) == 3);
    }
    
     @Test
    public void testGetPriceCodeChildrenMovieDeuxJours() {
        Movie m = new ChildrenMovie("Test");
        assertTrue(m.getPriceByDay(2) == 1.50);
    }
    


    @Test
    public void testGetPriceCodeNewReleaseMovie() {
        Movie m = new NewReleaseMovie("Test");
        assertTrue(m.getPriceByDay(3) == 9);
    }

}
