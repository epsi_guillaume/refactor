package com.mycompany.refactor;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

    @Test
    public void testGetName() {
        Customer c = new Customer("c1");
        assertEquals(c.getName(), "c1");
    }

    @Test
    public void testStatement() {
        Customer c = new Customer("c1");

        c.addRentals(new Rental(new RegularMovie("M1"), 10));
        c.addRentals(new Rental(new NewReleaseMovie("M3"), 5));

        String result = "Historique de location pour c1\n" + "\tM1\t14.0\n" + "\tM3\t15.0\n" + "Le montant total est 29.0\n"
                + "Vous avez gagné 3 points de location";
        assertEquals(c.statement(), result);
        
    }

}
