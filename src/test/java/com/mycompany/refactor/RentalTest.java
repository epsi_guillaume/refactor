package com.mycompany.refactor;

import static org.junit.Assert.*;

import org.junit.Test;

public class RentalTest {

    @Test
    public void testGetDaysRentedEquals() {
        Rental r = new Rental(null, 2);
        assertEquals(r.getDaysRented(), 2);
    }

    @Test
    public void testGetDaysRentedNotEquals() {
        Rental r = new Rental(null, 2);
        assertNotEquals(r.getDaysRented(), 3);
    }

    @Test
    public void testGetMovieEquals() {
        Movie m1 = new RegularMovie("m1");
        Rental r = new Rental(m1, 2);
        assertEquals(r.getMovie(), m1);
    }

    @Test
    public void testGetMovieNotEquals() {
        Movie m1 = new RegularMovie("m1");
        Movie m2 = new ChildrenMovie("m2");
        Rental r = new Rental(m1, 2);
        assertNotEquals(r.getMovie(), m2);
    }

    @Test
    public void testAmoutFor() {
        Movie m1 = new RegularMovie("m1");
        Rental r = new Rental(m1, 2);
        assertTrue(r.amoutFor() == 2);
    }

}
