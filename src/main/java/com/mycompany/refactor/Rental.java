package com.mycompany.refactor;

public class Rental {

    private int daysRented;
    private Movie movie;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }


    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    double amoutFor() {
        return movie.getPriceByDay(daysRented);
    }
    
    public int getFrequentRenterPoints(){
    	return movie.getFrequentRenterPoints();
    }
    @Override
    public String toString() {
    	return "\t" + getMovie().getTitle()
				+ "\t" + amoutFor()+ "\n";
    }
}


