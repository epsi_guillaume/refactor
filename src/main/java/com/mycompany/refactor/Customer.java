package com.mycompany.refactor;

import java.util.*;
public class Customer {
	private java.lang.String name;
	private List<Rental> rentals;

	public Customer(String name) {
		this.name = name;
		this.rentals = new ArrayList<>();
	}

	public void addRentals(Rental arg) {
		this.rentals.add(arg);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;
                
                StringBuilder bld = new StringBuilder();
                bld.append("Historique de location pour ").append(getName()).append("\n");
		
                
		for(Rental r : rentals){
			totalAmount += r.amoutFor();
			bld.append(r.toString());
			frequentRenterPoints += r.getFrequentRenterPoints();
		}
		
		// add footer lines
		bld.append("Le montant total est ").append(String.valueOf(totalAmount)).append("\n");
		bld.append("Vous avez gagné ").append(String.valueOf(frequentRenterPoints)).append(" points de location");

		return bld.toString();
	}

}