package com.mycompany.refactor;

public class RegularMovie extends Movie {

    public RegularMovie(String title) {
        super(title);
    }

    @Override
    public double getPriceByDay(int day) {
        double amount = 2;
        if (day > 2) {
            amount += (day - 2) * 1.5;
        }
        return amount;
    }

    @Override
    public int getFrequentRenterPoints() {
        return 1;
    }

}
