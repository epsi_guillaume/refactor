package com.mycompany.refactor;

public class ChildrenMovie extends Movie {

    public ChildrenMovie(String title) {
        super(title);
    }

    @Override
    public double getPriceByDay(int day) {
        double amount = 1.5;
        if (day > 3) {
            amount += (day - 3) * 1.5;
        }
        return amount;
    }

    @Override
    public int getFrequentRenterPoints() {
        return 1;
    }

}
