package com.mycompany.refactor;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {
    
    private static Logger logger = Logger.getLogger("Main.class");

    public static void main(String[] a){
        Movie m1 = new NewReleaseMovie("Le Seigneur des Anneaux");
        Movie m2 = new RegularMovie("Matrix");
        Movie m3 = new ChildrenMovie("Star Wars");
        
        Customer c1 = new Customer("Moi, un fidèle client");
        
        Rental r1 = new Rental(m1, 5);
        Rental r2 = new Rental(m2, 1);
        Rental r3 = new Rental(m3, 3);
        
        c1.addRentals(r1);
        c1.addRentals(r2);
        c1.addRentals(r3);
        
        logger.log(Level.INFO, "", c1.statement());
        System.err.println(c1.statement());
    }

}
