package com.mycompany.refactor;


public class NewReleaseMovie extends Movie{

	public NewReleaseMovie(String title) {
		super(title);
	}

	@Override
	public double getPriceByDay(int day) {
		return 3f * day;
	}

	@Override
	public int getFrequentRenterPoints() {
		return 2;
	}

}
